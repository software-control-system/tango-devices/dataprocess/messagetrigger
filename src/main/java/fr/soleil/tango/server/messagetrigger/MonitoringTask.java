package fr.soleil.tango.server.messagetrigger;

import fr.esrf.Tango.DevFailed;
import fr.esrf.Tango.DevState;
import fr.soleil.tango.clientapi.TangoDevice;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tango.utils.DevFailedUtils;

import java.util.*;

/**
 * Task to monitor the attributes
 */
final public class MonitoringTask extends TimerTask {

    // Members
    final private Timer timer;
    final private List<AttributeParser> monitoredAttributesList;
    final private Map<String, TangoDevice> monitoredDevicesMap;
    final private TangoDevice textTalkerDevice;
    final private MessagesQueue sentMessagesQueue;
    final private StateUpdates errorHandler;
    private final Logger logger = LoggerFactory.getLogger(MessageTrigger.class);

    // Variables for temporary stockage
    private String tmpMessage;
    private DevState tmpState;
    private boolean isStatusClean;
    private String infoString;
    private String alarmString;

    /**
     * Constructor
     * @param pollingPeriod
     * @param monitoredAttributesList
     * @param monitoredDevicesMap
     * @param textTalkerDevice
     * @param sentMessagesQueue
     * @param errorHandler
     */
    public MonitoringTask(int pollingPeriod,
                             List<AttributeParser> monitoredAttributesList,
                             Map<String, TangoDevice> monitoredDevicesMap,
                             TangoDevice textTalkerDevice,
                             MessagesQueue sentMessagesQueue,
                             StateUpdates errorHandler) {
        // Passing members
        this.textTalkerDevice = textTalkerDevice;
        this.monitoredAttributesList = monitoredAttributesList;
        this.errorHandler = errorHandler;
        this.sentMessagesQueue = sentMessagesQueue;
        this.monitoredDevicesMap = monitoredDevicesMap;
        infoString = new String();
        alarmString = new String();
        
        isStatusClean = false;

        // Starting polling
        timer = new Timer();
        timer.scheduleAtFixedRate(this, 0, pollingPeriod);
    }

    /**
     * Stops the task and purge it
     */
    public void stop() {
        // Canceling timer and task
        timer.cancel();
        timer.purge(); // Flag the timer as ready to be recycled
        this.cancel();
    }

    /**
     * Polled method
     */
    @Override
    public void run() {
        
        infoString = "";
        alarmString = "";

        // Checking text talker state
        try {
            tmpState = textTalkerDevice.state();
        } catch (DevFailed e) {
            errorHandler.sendAlarm("Cannot reach text talker\n");
            logger.warn(DevFailedUtils.toString(e));
            isStatusClean = false;
            return;
        }
        if (tmpState == DevState.FAULT || tmpState == DevState.ALARM) {
            errorHandler.sendAlarm("Text Talker Device is in " + tmpState.toString() + " state!\n");
            isStatusClean = false;
            return;
        }

        // Checking states of monitored devices
        for (String device_name : monitoredDevicesMap.keySet()) {
            // Try to get the state
            try {
                tmpState = monitoredDevicesMap.get(device_name).state();
            } catch (DevFailed e) {
                errorHandler.sendAlarm("Cannot reach monitored device " + device_name + "!\n");
                logger.warn(DevFailedUtils.toString(e));
                isStatusClean = false;
                return;
            }

            // Check for invalid states
            if (tmpState == DevState.FAULT) {
                errorHandler.sendAlarm( "Monitored device is in " + tmpState.toString() + " state!\n" + "Device: " + device_name + "\n");
                isStatusClean = false;
                return;
            }

            if (tmpState == DevState.ALARM) {
                infoString += "Monitored device " + device_name + " is in " + tmpState.toString() + " state.\n";
            }
        }

        // Going through monitored attributes  
        for (int i = 0; i < monitoredAttributesList.size(); i++) {
            // Try to check the current attribute
            try {
                tmpMessage = monitoredAttributesList.get(i).checkAttribute();
                
                // If there is a message to be sent
                if (!(tmpMessage.contentEquals("NULL"))) {
                    // Try to send the message
                    try {
                        textTalkerDevice.getTangoCommand("DevTalk").execute(String.class, tmpMessage);
                        sentMessagesQueue.pushMessage(tmpMessage);
                        infoString += "Detected update on attribute: " + monitoredAttributesList.get(i).getName() + "\n" +
                                        "Sent message: " + tmpMessage + "\n";
                    } catch (DevFailed e) {
                        errorHandler.sendFault("Cannot write message on Text Talker\n");
                        logger.error(DevFailedUtils.toString(e));
                        isStatusClean = false;
                        return;
                    } catch (NullPointerException e) {
                        errorHandler.sendFault("MessagesQueue: Cannot add message to the queue\n");
                        isStatusClean = false;
                        return;
                    } catch (InterruptedException e) {
                        errorHandler.sendFault("MessagesQueue: Monitoring thread got interrupted when adding message to the queue\n");
                        isStatusClean = false;
                        return;
                    }
                }
            } catch (DevFailed e) {
                String str = "Cannot check attribute " + monitoredAttributesList.get(i).getName();
                logger.warn(str, e);
                alarmString += str + "\n";
            }
        }
        
        // Send the string to status
        if(!alarmString.contentEquals("")) {
            errorHandler.sendAlarm(infoString + alarmString);
            isStatusClean = false;
            return;
        }
        
        // Send the string to status
        if(!infoString.contentEquals(""))
        {
            errorHandler.sendInfo(infoString);
            isStatusClean = false;
            return;
        }
        
        if(!isStatusClean) {
            errorHandler.cleanStatus();
            isStatusClean = true;
        }
        
    }
}
