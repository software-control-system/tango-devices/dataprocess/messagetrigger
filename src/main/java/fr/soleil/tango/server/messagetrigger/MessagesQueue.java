package fr.soleil.tango.server.messagetrigger;

import java.util.Date;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.TimeUnit;

/**
 * Class to handle the list of messages as an array of a fixed size
 */
final public class MessagesQueue extends ArrayBlockingQueue<String>{


    private static final long serialVersionUID = 7430487378383939300L;
    private static final int MSG_LOG_SIZE = 100;
    private static final int BLOCKING_TIME = 30; // Maximum time the thread can be blocked for, in ms.
    // Min polling time is 100ms, to be taken into account to fic this blocking time.
    
    /**
     * Constructor
     */
    public MessagesQueue() {
        super(MSG_LOG_SIZE);
    }
    

    /**
     * Adds a new message to the array
     *
     * @param message
     * @throws InterruptedException 
     * @throws NullPointerException 
     */
    public void pushMessage(String message) throws InterruptedException, NullPointerException  {
        // If we reached max size
        if (this.size() >= MSG_LOG_SIZE) {
            // Remove oldest message (thread can be blocked for BLOCKING_TIME max)
            final String element = this.poll(BLOCKING_TIME, TimeUnit.MILLISECONDS);
            // If it failed:
            if(element == null) {
                throw new NullPointerException();
            }
        }
        
        // Get current date & time
        Date date = new Date();

        // Add new message (thread can be blocked for BLOCKING_TIME max)
        final boolean isInserted = this.offer(date.toString() + "\n" + message + "\n", BLOCKING_TIME, TimeUnit.MILLISECONDS);
        if (!isInserted) {
            // This should never happen as we made space in the queue
            throw new NullPointerException();
        }
    }

    /**
     * Clears the array
     */
    public void clearMessages() {
        this.clear();
    }

    /**
     * Get the message array
     * 
     * @return message Array
     */
    public String[] getMessages() {
        return this.toArray(new String[0]);
    }

}
