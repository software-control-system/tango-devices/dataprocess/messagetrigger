package fr.soleil.tango.server.messagetrigger;


import fr.esrf.Tango.DevFailed;
import fr.soleil.tango.clientapi.TangoDevice;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tango.DeviceState;
import org.tango.server.ServerManager;
import org.tango.server.annotation.*;
import org.tango.utils.DevFailedUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * MessageTrigger Tango-Controls server
 *
 * @author Antonin Hottois <hottois@synchrotron-soleil.fr>
 * @since Wed Apr 01 17:35:47 CEST 2020
 */
@Device
public class MessageTrigger implements StateUpdates {


    private final Logger logger = LoggerFactory.getLogger(MessageTrigger.class);
    private final List<AttributeParser> monitoredAttributesList = new ArrayList<>();
    private final Map<String, TangoDevice> monitoredDevicesMap = new HashMap<String, TangoDevice>();
    private MonitoringTask triggerTask;
    private MessagesQueue sentMessagesQueue;
    private TangoDevice textTalkerDevice;
    private String monitoredAttributesString;
    
    
    // Property MessageList
    @DeviceProperty(defaultValue = {"test/test/Parser/Attribute1;MessageIfTrue;MessageIfFalse"},
                    isMandatory = true,
                    description = "List of the attributes and messages to be sent.\n" +
                    "For example: test/test/parser/Attribute1;MessageIfTrue;MessageIfFalse\n" +
                    "Messages are optional: write NULL to not send a message. For example: attrib1;Alert attrib1 is now true!;NULL\n")
    private String[] MessageList;
    
    public void setMessageList(String[] MessageList) {
        this.MessageList = MessageList;
    }
    
    
    // Property TextTalker
    @DeviceProperty(description = "Name/Path of the Text Talker Proxy in the database.\nFor example: test/test/talker", isMandatory = true)
    private String TextTalkerProxyName;
    
    public void setTextTalkerProxyName(String TextTalkerProxyName) {
        this.TextTalkerProxyName = TextTalkerProxyName;
    }
    
    
    // Property PollingPeriod
    @DeviceProperty(defaultValue = "1000", description = "Polling period in milliseconds.\nMinimum is 100 milliseconds.")
    private int PollingPeriod;
    
    public void setPollingPeriod(int PollingPeriod) {
        this.PollingPeriod = PollingPeriod;
    }
    
    
    // State
    @State
    private volatile DeviceState state = DeviceState.INIT;
    
    public DeviceState getState() { 
        return state;
    }

    // This might be accessed from the monitoring thread, hence the synchronization
    public synchronized void setState(DeviceState state) {
        this.state = state;
    }
    
    
    // Status
    @Status
    private String status = "Initializing Device";

    public String getStatus() {
        return status;
    }

    // This might be accessed from the monitoring thread, hence the synchronization
    public synchronized void setStatus(String status) {
        this.status = status;
    }
    
    // Main
    public static void main(String[] args) {
        ServerManager.getInstance().start(args, MessageTrigger.class);
    }
    
    // Delete
    @Delete
    public void delete() {
        if (triggerTask != null) triggerTask.stop();
    }


    // Init
    @Init
    public void init() {
        setState(DeviceState.INIT);
        setStatus("Initializing...");
        

        // Initializing message list
        sentMessagesQueue = new MessagesQueue();

        // Checking polling period
        if (PollingPeriod < 100) {
            sendFault("Invalid PollingPeriod property! It should be at least 100ms.");
            return;
        }

        // Initializing TextTalker device
        try {
            textTalkerDevice = new TangoDevice(TextTalkerProxyName);   
        } 
        catch (DevFailed e) {
            sendFault("Cannot reach Text Talker Proxy. Check the property TextTalkerProxyName.");
            logger.error(DevFailedUtils.toString(e));
            return;
        }



        // Variables for temporary work
        AttributeParser attribute;
        String device_name;
        
        // This should be cleared (in case of init when the device is already on for example)
        monitoredDevicesMap.clear();
        monitoredAttributesList.clear();
        

        // Retrieving all attributes from message list
        for(int i=0; i<MessageList.length; i++)
        {
            try
            {
                // Create a new attribute
                attribute = new AttributeParser(MessageList[i]);
                // Add it to our list
                monitoredAttributesList.add(attribute);
            }
            catch (DevFailed e) 
            {
                sendFault("Error when initializing monitored attributes.\n" + "Check line "+ (i+1) +" of property MessageList.");
                logger.error(DevFailedUtils.toString(e));
                return;
            }
            
            // Get the device name of current attribute
            device_name = attribute.getDeviceName().toLowerCase();
            
            // If not already retrieved, get the device
            if(!monitoredDevicesMap.containsKey(device_name))
            {
                try
                {
                    monitoredDevicesMap.put(device_name, new TangoDevice(device_name));
                }
                catch (DevFailed e)
                {
                    sendFault("Cannot reach device " + device_name);
                    logger.error(DevFailedUtils.toString(e));
                    return;
                }
            }
        }
        
        // Get the list of monitored attributes to display their names in status
        monitoredAttributesString = "Monitored attributes:\n";
        for (int i = 0; i < monitoredAttributesList.size(); i++) {
            monitoredAttributesString += monitoredAttributesList.get(i).getName();
            monitoredAttributesString += "\n";
        }

        // If we reach this, init went fine.
        // Device should be OFF until it's started.
        setState(DeviceState.OFF);
        setStatus("Ready! Standing by...");
        logger.info("Successfull init!");

        // Starting by default
        Start();
    }


    // Attribute SentMessageList
    @Attribute
    public String[] getSentMessageList() {
        return sentMessagesQueue.getMessages();
    }

    // Command Stop
    @Command(inTypeDesc = "Stops the monitoring service", displayLevel = 1)
    @StateMachine(deniedStates = {DeviceState.FAULT, DeviceState.OFF, DeviceState.INIT})
    public void Stop() {
        logger.info("Stopping...");
        // Stops the monitoring task
        if (triggerTask != null) triggerTask.stop();
        setStatus("Trigger service stopped!\nDevice standing by...");
        setState(DeviceState.OFF);
    }

    // Command Start
    @Command(inTypeDesc = "Starts the monitoring service", displayLevel = 1)
    @StateMachine(deniedStates = {DeviceState.FAULT, DeviceState.ALARM, DeviceState.ON, DeviceState.INIT})
    public void Start() {
        logger.info("Starting...");
        // Starts the monitoring task
        triggerTask = new MonitoringTask(PollingPeriod, monitoredAttributesList, monitoredDevicesMap, textTalkerDevice, sentMessagesQueue, this);
        setStatus("Trigger service started!\n\n" + monitoredAttributesString);
        setState(DeviceState.ON);
    }

    // Command ClearMessagesList
    @Command(inTypeDesc = "Clear the list of messages")
    @StateMachine(deniedStates = {DeviceState.FAULT})
    public void ClearMessagesList() {
        sentMessagesQueue.clearMessages();
    }

    // FAULT: fatal error: stops the trigger task, lock the device in FAULT state, log the error.
    @Override
    public void sendFault(String desc) {
        if (triggerTask != null) triggerTask.stop();
        logger.error(desc);

        setState(DeviceState.FAULT);
        setStatus(desc + "\n" + "Use init to restart!");    // +"\n\n"+DevFailedUtils.toString(e));
    }

    // ALARM: warning: logs the error and set the device in ALARM state.
    @Override
    public void sendAlarm(String desc) {
        
        final String[] tmp = desc.split("\n");
        for(String str : tmp) {
            logger.warn(str);
        }
  
        setState(DeviceState.ALARM);
        setStatus(desc);
    }

    // INFO: update status and log the event
    @Override
    public void sendInfo(String desc) {
        
        final String[] tmp = desc.split("\n");
        for(String str : tmp) {
            logger.info(str);
        }
        
        setState(DeviceState.ON);
        setStatus(desc + "\n" + monitoredAttributesString);
    }

    // Reset the status
    @Override
    public void cleanStatus() {
        setState(DeviceState.ON);
        setStatus(monitoredAttributesString);
    }
}