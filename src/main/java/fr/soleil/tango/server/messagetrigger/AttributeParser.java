package fr.soleil.tango.server.messagetrigger;

import fr.esrf.Tango.DevFailed;
import fr.soleil.tango.clientapi.TangoAttribute;
import org.tango.utils.DevFailedUtils;

/**
 * Class to handle each attribute and its messages
 */
final public class AttributeParser {

    // Members
    final private TangoAttribute currentAttribute;
    final private String trueMessage;
    final private String falseMessage;
    final private String deviceName;

    private boolean previousState;

    /**
     * Constructor
     *
     * @param messageString
     * @throws DevFailed
     */
    public AttributeParser(String messageString) throws DevFailed {
        // Split the message
        String[] tokens = messageString.split(";");

        // Check for its length
        if (tokens.length != 3) {
            throw DevFailedUtils.newDevFailed("MessageList property is not formated properly! Check the number of separators (;)");
        }

        // Get a new currentAttribute from first part of the string
        currentAttribute = new TangoAttribute(tokens[0]);

        // Get the "message_if_true"
        if (tokens[1].contentEquals("") || tokens[1].equalsIgnoreCase("NULL") || tokens[2].matches("^\\s*$")) {
            trueMessage = "NULL";
        } else {
            trueMessage = tokens[1];
        }

        // Check the "message_if_false"
        if (tokens[2].contentEquals("") || tokens[2].equalsIgnoreCase("NULL") || tokens[2].matches("^\\s*$")) {
            falseMessage = "NULL";
        } else {
            falseMessage = tokens[2];
        }

        // Initialize previous state variable
        previousState = currentAttribute.read(boolean.class);

        // Get the name of the device the currentAttribute is from
        deviceName = currentAttribute.getDeviceName();
    }

    /**
     * Check if the attribute has changed and return the corresponding message
     *
     * @return Return the message if attribute has changed, return "NULL" if no message to be send
     * @throws DevFailed
     */
    public String checkAttribute() throws DevFailed {
        // Reading currentAttribute as boolean
        boolean result = currentAttribute.read(boolean.class);

        // Has the currentAttribute changed since last time?
        if (result != previousState) {
            previousState = result;
            return result ? trueMessage : falseMessage;
        }
        return "NULL";
    }

    /**
     * Get Attribute name
     *
     * @return Return the name of current attribute
     */
    public String getName() {
        return currentAttribute.getName();
    }

    /**
     * Get Device name
     *
     * @return Return the name of the device current attribute is from.
     */
    public String getDeviceName() {
        return deviceName;
    }
}
