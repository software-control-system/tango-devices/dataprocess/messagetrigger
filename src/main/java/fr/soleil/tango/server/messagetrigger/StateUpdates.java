package fr.soleil.tango.server.messagetrigger;


/**
 * Interface so that the monitoring task can send errors to the device
 */
public interface StateUpdates 
{
    void cleanStatus();
    void sendFault(String desc);
    void sendAlarm(String desc);
    void sendInfo(String desc);
}
