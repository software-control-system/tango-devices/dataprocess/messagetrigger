@ECHO OFF
GOTO SCRIPT_BEGIN

:USAGE

   ECHO Usage: myJavaDeviceServer.bat instance_name  [-vverbose_level]
   ECHO   instance_name: the server instance name (as registered in the TANGO database) 
   ECHO   verbose_level: optional verbose level (e.g. -v1 for info, -v4 for debug) 
   GOTO   SCRIPT_END



:SCRIPT_BEGIN

IF NOT DEFINED TANGO_HOST (
 ECHO TANGO_HOST is not defined. Aborting!
 ECHO Please define a TANGO_HOST env. var. pointing to your TANGO database.
 ECHO TANGO_HOST syntax is tango_database_host::tango_database_port [e.g. venus::20000].
 GOTO SCRIPT_END
)

IF {%1} == {} (
 ECHO Error: no instance name specified.
 ECHO . 
 GOTO USAGE
)


 
:: classe principale du device server
# classe principale du device server
set MAIN_CLASS=fr.soleil.tango.server.messagetrigger.MessageTrigger


:: CLASSPATH
:: le repertoire contenant les device servers java et leurs dependances
:: generalement \\DeviceServers\java\
set DEVICE_ROOT_JAVA=%~dp0%

set  CLASSPATH=%CLASSPATH%;%DEVICE_ROOT_JAVA%/fr.soleil.deviceservers-MessageTrigger-1.1.0.jar
set  CLASSPATH=%CLASSPATH%;%DEVICE_ROOT_JAVA%/org.tango-controls-JTangoServer-9.5.19.jar
set  CLASSPATH=%CLASSPATH%;%DEVICE_ROOT_JAVA%/org.tango-controls-TangORB-9.5.19.jar
set  CLASSPATH=%CLASSPATH%;%DEVICE_ROOT_JAVA%/org.tango-controls-JTangoClientLang-9.5.19.jar
set  CLASSPATH=%CLASSPATH%;%DEVICE_ROOT_JAVA%/ch.qos.logback-logback-classic-1.2.3.jar
set  CLASSPATH=%CLASSPATH%;%DEVICE_ROOT_JAVA%/org.slf4j-slf4j-ext-1.7.6.jar
set  CLASSPATH=%CLASSPATH%;%DEVICE_ROOT_JAVA%/net.sf.transmorph-transmorph-3.1.3.jar
set  CLASSPATH=%CLASSPATH%;%DEVICE_ROOT_JAVA%/net.sf.ehcache-ehcache-core-2.3.0.jar
set  CLASSPATH=%CLASSPATH%;%DEVICE_ROOT_JAVA%/org.quartz-scheduler-quartz-2.2.1.jar
set  CLASSPATH=%CLASSPATH%;%DEVICE_ROOT_JAVA%/org.apache.commons-commons-math3-3.5.jar
set  CLASSPATH=%CLASSPATH%;%DEVICE_ROOT_JAVA%/org.jacorb-jacorb-3.8.jar
set  CLASSPATH=%CLASSPATH%;%DEVICE_ROOT_JAVA%/org.jacorb-jacorb-services-3.8.jar
set  CLASSPATH=%CLASSPATH%;%DEVICE_ROOT_JAVA%/org.zeromq-jeromq-0.3.4.jar
set  CLASSPATH=%CLASSPATH%;%DEVICE_ROOT_JAVA%/org.tango-controls-JTangoCommons-9.5.19.jar
set  CLASSPATH=%CLASSPATH%;%DEVICE_ROOT_JAVA%/org.apache.commons-commons-lang3-3.7.jar
set  CLASSPATH=%CLASSPATH%;%DEVICE_ROOT_JAVA%/org.mozilla-rhino-1.7.7.1.jar
set  CLASSPATH=%CLASSPATH%;%DEVICE_ROOT_JAVA%/ch.qos.logback-logback-core-1.2.3.jar
set  CLASSPATH=%CLASSPATH%;%DEVICE_ROOT_JAVA%/c3p0-c3p0-0.9.1.1.jar
set  CLASSPATH=%CLASSPATH%;%DEVICE_ROOT_JAVA%/org.jacorb-jacorb-omgapi-3.8.jar
set  CLASSPATH=%CLASSPATH%;%DEVICE_ROOT_JAVA%/antlr-antlr-2.7.2.jar
set  CLASSPATH=%CLASSPATH%;%DEVICE_ROOT_JAVA%/mx4j-mx4j-3.0.1.jar
set  CLASSPATH=%CLASSPATH%;%DEVICE_ROOT_JAVA%/nanocontainer-nanocontainer-remoting-1.0-RC-1.jar
set  CLASSPATH=%CLASSPATH%;%DEVICE_ROOT_JAVA%/org.beanshell-bsh-2.0b5.jar
set  CLASSPATH=%CLASSPATH%;%DEVICE_ROOT_JAVA%/picocontainer-picocontainer-1.2.jar
set  CLASSPATH=%CLASSPATH%;%DEVICE_ROOT_JAVA%/tanukisoft-wrapper-3.1.0.jar
set  CLASSPATH=%CLASSPATH%;%DEVICE_ROOT_JAVA%/org.tango-controls-tango-idl-java-5.jar
set  CLASSPATH=%CLASSPATH%;%DEVICE_ROOT_JAVA%/com.google.guava-guava-20.0.jar
set  CLASSPATH=%CLASSPATH%;%DEVICE_ROOT_JAVA%/org.jacorb-jacorb-idl-compiler-3.8.jar
set  CLASSPATH=%CLASSPATH%;%DEVICE_ROOT_JAVA%/org.apache.ant-ant-1.8.2.jar
set  CLASSPATH=%CLASSPATH%;%DEVICE_ROOT_JAVA%/java_cup-java_cup-0.9e.jar
set  CLASSPATH=%CLASSPATH%;%DEVICE_ROOT_JAVA%/org.apache.ant-ant-launcher-1.8.2.jar
set  CLASSPATH=%CLASSPATH%;%DEVICE_ROOT_JAVA%/ch.qos.cal10n-cal10n-api-0.8.1.jar
set  CLASSPATH=%CLASSPATH%;%DEVICE_ROOT_JAVA%/org.slf4j-slf4j-api-1.7.25.jar


:: crash path
for /f "tokens=2-4 delims=/ " %%a in ('date /T') do set year=%%c
for /f "tokens=2-4 delims=/ " %%a in ('date /T') do set month=%%a
for /f "tokens=2-4 delims=/ " %%a in ('date /T') do set day=%%b
set TODAY=%year%-%month%-%day%

for /f "tokens=1 delims=: " %%h in ('time /T') do set hour=%%h
for /f "tokens=2 delims=: " %%m in ('time /T') do set minutes=%%m
for /f "tokens=3 delims=: " %%a in ('time /T') do set ampm=%%a
set NOW=%hour%-%minutes%-%ampm%

SET CRASH_PATH=%TANGO_JAVA_CRASH_PATH%\%MAIN_CLASS%-%1-%TODAY%-%NOW%

:: OPTIONS de VM
set JAVA_OPTIONS=-Dcom.sun.management.jmxremote=true -Djava.awt.headless=true

:: lancement du device server
javaw %JAVA_OPTIONS% -cp %CLASSPATH% -DTANGO_HOST=%TANGO_HOST% -XX:+HeapDumpOnOutOfMemoryError -XX:HeapDumpPath=%CRASH_PATH%.hprof -XX:ErrorFile=%CRASH_PATH%.log %MAIN_CLASS% %1 %2
  

:SCRIPT_END