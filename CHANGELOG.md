## [1.1.0] - 17/07/2020

### First release

### Changed
- Added parent SOLEIL pom

## [1.0.8] - 09/07/2020

### Changed
- Device no longer go in FAULT state when monitored attribute can't be reached while Parser is reachable
- Changed logging and status handling in the monitoring task

## [1.0.7] - 02/07/2020

### Changed
- The messages are now stored in a BlockingQueue (handling concurrency)
- Modified some logging and status handling

## [1.0.6] - 02/07/2020

### Changed
- TextTalker not being reachable no longer stop the monitoring task
- TextTalker not being in a valid state no longer stop the monitoring task
- sent message list is now handled by a Queue class
- Init, stop and starts are now logged

## [1.0.5] - 23/06/2020

### This version is the result of actual tests within RCM1
### Most changes are the result of an overhaul of the error handling system.

### Changed
- Start/Stop commands are now expert level.
- Monitoring task is now started at init.
- Parser sub-devices being in Alarm mode no longer stop the monitoring task.
- Parser sub-devices not answering no longer stop the monitoring task.
- Changed jar output name to match SOLEIL standards.
- Changed package name to match SOLEIL standards.
- Modified README.MD.

### Added
- Device is now in ALARM mode when Parser sub-devices are not answering.
- Added Changelog.
- Added pom_fat_jar.xml to generate a jar with dependencies.
- Added MessageTrigger scripts to use the jar without dependencies within the control network.

## [1.0.4] - 22/06/2020

### This version was not committed, it was developed during the tests within RCM1

## [1.0.3] - 11/05/2020

### Overhauled the whole communication system with TangoParsers, allowing for monitoring of several parsers at the same time.

### Removed
- Removed "TangoParserProxy" property, as it is no longer necessary

## [1.0.2] - 07/06/2020

### Changed
- LastStates are now initialized with the actual states at init, so that no message is sent during init.
- Changed the way "MessageList" property is parsed

### Added
- Commented the code

### Removed
- Removed "LastRecordedState" property, as it is useless in this new MessageTrigger.

## [1.0.1] - 30/04/2020
### First version of the device