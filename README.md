# Welcome to Tango-Controls server MessageTrigger v1.1.0

## Description

The MessageTrigger is a device design to poll regularly the attributes of TangoParser sub devices, and send a message to a TextTalker sub device if a change occured.

It is possible to monitor several TangoParser devices with one MessageTrigger device. The configuration is done through the properties of the device:

### MessageList property

This property contains the list of attributes to monitor and the messages to send. This property is mandatory.

```
test/test/parser1/attribute1;Attribute 1 is true!;Attribute 1 is false!;
test/test/parser1/attribute2;Attribute 2 is true!;Attribute 2 is false!;
```

### PollingPeriod property

This property is the polling period of the monitoring, in ms. This property is not mandatory, default period is 1s.

### TextTalkerProxyName

This property should contain the address of the TextTalker device the message will be sent to. This property is mandatory.

## Usage

Clone this repo and build an executable jar with dependencies using maven:

```bash
$> mvn package -f pom_fat_jar.xml
```

Run the server:

```bash
$> java -jar target/MessageTrigger-1.1.0-jar-with-dependencies.jar

```

Or build the jar without dependencies for integration in the control system:

```bash
$> mvn package
```

You may then use the following files for integration:

Linux:

```bash
$> ./MessageTrigger
```

Windows:

```bash
$> MessageTrigger.bat
```